import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobilenumberpageComponent } from './mobilenumberpage.component';

describe('MobilenumberpageComponent', () => {
  let component: MobilenumberpageComponent;
  let fixture: ComponentFixture<MobilenumberpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobilenumberpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobilenumberpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
