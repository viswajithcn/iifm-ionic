import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecurityserviceComponent } from './securityservice.component';

describe('SecurityserviceComponent', () => {
  let component: SecurityserviceComponent;
  let fixture: ComponentFixture<SecurityserviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecurityserviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecurityserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
