import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IifmspageComponent } from './iifmspage.component';

describe('IifmspageComponent', () => {
  let component: IifmspageComponent;
  let fixture: ComponentFixture<IifmspageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IifmspageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IifmspageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
