import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { SignupComponent } from './signup/signup.component';
import { Signup2Component } from './signup2/signup2.component';
import { LanguageComponent } from './language/language.component';
import { LocationComponent } from './location/location.component';
import { OtpComponent } from './otp/otp.component';
import { WelcomepageComponent } from './welcomepage/welcomepage.component';
import { IifmspageComponent } from './iifmspage/iifmspage.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { MaidComponent } from './maid/maid.component';
import { CharacterComponent } from './character/character.component';
import { SecurityserviceComponent } from './securityservice/securityservice.component';
import { MobilenumberpageComponent } from './mobilenumberpage/mobilenumberpage.component';
import { HomepageComponent } from './homepage/homepage.component';


const appRoutes: Routes = [
  // { path: '', component: HomePage },
  { path: 'language', component: LanguageComponent },
  {path: 'location', component: LocationComponent },
  { path: 'IifmspageComponent', component: IifmspageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'signup2', component: Signup2Component },
  { path: 'language', component: LanguageComponent },
  { path: 'otp', component: OtpComponent },
  { path: 'welcomepage', component: WelcomepageComponent },
  { path: 'iifmspage', component: IifmspageComponent },
  { path: 'resetpassword', component: ResetpasswordComponent },
  { path: 'maid', component: MaidComponent },
  { path: 'character', component: CharacterComponent },
  { path: 'securityservice', component: SecurityserviceComponent },
  { path: 'mobilenumberpage', component: MobilenumberpageComponent },
  {path: 'homepage', component: HomepageComponent }
];


@NgModule({
  declarations: [AppComponent,
    LoginComponent,
    HomeComponent,
    SignupComponent,
    Signup2Component,
    LanguageComponent,
    LocationComponent,
    OtpComponent,
    WelcomepageComponent,
    IifmspageComponent,
    ResetpasswordComponent,
    MaidComponent,
    CharacterComponent,
    SecurityserviceComponent,
    MobilenumberpageComponent,
    HomepageComponent ],
  entryComponents: [],
  imports: [ RouterModule.forRoot(appRoutes,),  BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
